﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening.Plugins.Options;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public UIManager UIManager;
    public PersistantMemory PersistantMemory;
    public Level[] levels;

    private GameManager gameManager;

    [SerializeField] private int test_level = 0;
    private int _currentLevel = 0;
    private int _coin = 0;
    private int _highScore = 0;
    // Start is called before the first frame update
    void Start()
    {
        gameManager = Camera.main.transform.GetComponent<GameManager>();
        _coin = PersistantMemory.GetCoin();
        _highScore = PersistantMemory.GetHighScore();
        UIManager.OnSetCoin(_coin);
        UIManager.SetHighScore(_highScore);

    }

    public void InitialLevel()
    {
        _currentLevel = PersistantMemory.GetLevel();
#if UNITY_EDITOR
        if (test_level > 0 && test_level < levels.Length)
        {
            _currentLevel = test_level;
        }
#endif
        gameManager.InitialGame(levels[_currentLevel], GoNextLevel, RestartLevel);
    }

    public void GoNextLevel(int coin, int score)
    {
        _coin += coin;
        PersistantMemory.SetCoin(_coin);
        UIManager.OnSetCoin(_coin);
        if (score > _highScore)
        {
            _highScore = score;
            PersistantMemory.SetHighScore(score);
            UIManager.SetHighScore(_highScore);
        }
        
        if (levels.Length == _currentLevel + 1)
        {
            RestartLevel();
            return;
        }
        _currentLevel += 1;
        PersistantMemory.SetLevel(_currentLevel);
        Invoke("InitialLevel", 2f);
    }

    public void RestartLevel()
    {
        Invoke("InitialLevel", 2f);
    }
    
    
    
}
