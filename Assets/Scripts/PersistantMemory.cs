﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistantMemory : MonoBehaviour
{
    public int GetLevel()
    {
        if (PlayerPrefs.HasKey("CurrentLevel"))
        {
            return PlayerPrefs.GetInt("CurrentLevel");
        }
        else
        {
            PlayerPrefs.SetInt("CurrentLevel", 0);
            return 0;
        }
    }

    public void SetLevel(int level)
    {
        PlayerPrefs.SetInt("CurrentLevel", level);
    }
    
    public int GetCoin()
    {
        if (PlayerPrefs.HasKey("Coin"))
        {
            return PlayerPrefs.GetInt("Coin");
        }
        else
        {
            PlayerPrefs.SetInt("Coin", 0);
            return 0;
        }
    }

    public void SetCoin(int level)
    {
        PlayerPrefs.SetInt("Coin", level);
    }
    
    public int GetHighScore()
    {
        if (PlayerPrefs.HasKey("HighScore"))
        {
            return PlayerPrefs.GetInt("HighScore");
        }
        else
        {
            PlayerPrefs.SetInt("HighScore", 0);
            return 0;
        }
    }

    public void SetHighScore(int level)
    {
        PlayerPrefs.SetInt("HighScore", level);
    }
}
