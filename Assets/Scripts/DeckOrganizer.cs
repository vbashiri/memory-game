﻿//using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class DeckOrganizer : MonoBehaviour
{
    [SerializeField] private Transform cardPool;
    [SerializeField] private GameObject cardPrefab;
    [SerializeField] private ColorPalette[] colorPalettes;

    [SerializeField] private List<Transform> cards;

    private Vector2 basePosition = new Vector2(-3.3f, 5.8f);

    public void OrganizeDeck(Level level)
    {
        cards.Clear();
        int[] nums = GenerateRandomNumbers(level.cardsCount);
        int xIndex = 0;
        int yIndex = 0;
        for (int i = 0; i < level.cardsCount; i++)
        {
            Transform card;
            if (cardPool.childCount > i)
            {
                card = cardPool.GetChild(i);
            }
            else
            {
                card = Instantiate(cardPrefab, cardPool).transform;
            }
            card.DOMove(
                new Vector3(basePosition.x + 2.2f * xIndex, basePosition.y - 3.2f * yIndex, 0f),
                0.8f)
                .PlayForward();
            
            card.GetComponent<Card>().SetCardType(nums[i], level.colorPallete);
            cards.Add(card);
            xIndex += 1;
            if (xIndex > 3)
            {
                xIndex = 0;
                yIndex += 1;
            }
        }
    }

    public void RevealDeck(UnityAction OnRevealCompelete)
    {
        StartCoroutine(RevealRoutine(OnRevealCompelete));
    }

    public void CollectDeckCards(UnityAction onCollectCompelete)
    {
        StartCoroutine(CollectDeckRoutine(onCollectCompelete));
    }

    public int[] GenerateRandomNumbers(int cardNumber)
    {
        int[] nums = new int[cardNumber];
        for (int i = 0; i < cardNumber; i++)
        {
            nums[i] = i / 2;
        }
        Random rng = new Random();
        rng.Shuffle(nums);
        
        return nums;
    }

    public IEnumerator RevealRoutine(UnityAction OnCompelete)
    {
        foreach (var card in cards)
        {
            card.GetComponent<Card>().RevealCard();
            yield return new WaitForSecondsRealtime(0.05f);
        }

        OnCompelete();
    }
    
    public IEnumerator CollectDeckRoutine(UnityAction OnCompelete)
    {
        yield return new WaitForSecondsRealtime(1f);
        foreach (var card in cards)
        {
            card.DOLocalMove(new Vector3(0f, 0f, 0f), 0.8f).PlayForward();
            yield return new WaitForSecondsRealtime(0.1f);
        }

        OnCompelete();
    }

}

static class RandomExtensions
{
    public static void Shuffle<T> (this Random rng, T[] array)
    {
        int n = array.Length;
        for (int i=0; i < n - 1; i++)
        {
            int k = Random.Range(i, n);
            T temp = array[k];
            array[k] = array[i];
            array[i] = temp;
        }
        
    }
}
