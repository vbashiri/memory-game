﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Card : MonoBehaviour
{
    private const float ShowCardWaitTime = 1.5f;
    
    public int cardType = 0;
    public enum CardState
    {
     back,
     front,
     waitToBack,
     matched
    }
    public CardState cardState = CardState.back;
    
    [SerializeField] private Sprite cardsFront;
    [SerializeField] private Sprite cardsBack;

    private Color frontColor = Color.white;
    private GameManager _gameManager;
    private SpriteRenderer _spriteRenderer;
    private Sequence _flipCard;
    private Sequence _flipCardBack;
    private Sequence _disappearCard;
    private Coroutine _waitToFlipBackRoutine;


    void Awake()
    {
        _gameManager = Camera.main.transform.GetComponent<GameManager>();
        _spriteRenderer = transform.GetComponent<SpriteRenderer>();
        InitialAnimations();
    }

    public void SetCardType(int type, ColorPalette colorPalette)
    {
        cardState = CardState.back;
        frontColor = colorPalette.colors[type];
        InitialDisappearAnimation();
        _spriteRenderer.sprite = cardsBack;
        _spriteRenderer.color = Color.white;
        cardType = type;
    }

    public void OnClick()
    {
        if (cardState == CardState.matched)
        {
            return;
        }
        _gameManager.CardClick(this);
    }

    public void FlipBack()
    {
        if (cardState == CardState.back)
        {
            return;
        }

        if (cardState == CardState.waitToBack)
        {
            StopCoroutine(_waitToFlipBackRoutine);
        }

        cardState = CardState.back;
        _flipCard.Pause();
        _flipCardBack.Restart();
        _flipCardBack.Play();
    }
    
    public void FlipFront()
    {
        if (cardState == CardState.front)
        {
            return;
        }

        if (cardState == CardState.waitToBack)
        {
            StopCoroutine(_waitToFlipBackRoutine);
            cardState = CardState.front;
            return;
        }
        cardState = CardState.front;
        _flipCardBack.Pause();
        _flipCard.Restart();
        _flipCard.Play();
    }

    public void RevealCard()
    {
        FlipFront();
        FlipBackWithWait();
    }

    public void Matched()
    {
        if (cardState == CardState.back)
        {
            FlipFront();
        }

        if (cardState == CardState.waitToBack)
        {
            StopCoroutine(_waitToFlipBackRoutine);
        }
        _disappearCard.Restart();
        _disappearCard.PlayForward();
        cardState = CardState.matched;
    }


    public void FlipBackWithWait()
    {
        if (cardState == CardState.front)
        {
            _waitToFlipBackRoutine = StartCoroutine(FlipBackWithWaitRoutine());
        }
    }

    public IEnumerator FlipBackWithWaitRoutine()
    {
          cardState = CardState.waitToBack;
            yield return new WaitForSecondsRealtime(ShowCardWaitTime);
            FlipBack();
    }
    
    

    private void InitialAnimations()
    {
        InitialFlipAnimation();
        InitialFlipBackAnimation();
        InitialDisappearAnimation();
    }

    private void InitialFlipAnimation()
    {
        _flipCard = DOTween.Sequence();
        _flipCard.Append(transform.DOScale(new Vector2(0f, 1f), 0.2f));
        _flipCard.AppendCallback(() =>
        {
            _spriteRenderer.sprite = cardsFront;
            _spriteRenderer.color = frontColor;
        });
        _flipCard.Append(transform.DOScale(new Vector2(1f, 1f), 0.2f));
    }
    private void InitialFlipBackAnimation()
    {
        _flipCardBack = DOTween.Sequence();
        _flipCardBack.Append(transform.DOScale(new Vector2(0f, 1f), 0.2f));
        _flipCardBack.AppendCallback(() =>
        {
            _spriteRenderer.sprite = cardsBack;
            _spriteRenderer.color = Color.white;
        });
        _flipCardBack.Append(transform.DOScale(new Vector2(1f, 1f), 0.2f));
    }
    

    private void InitialDisappearAnimation()
    {
        _disappearCard = DOTween.Sequence();
        _disappearCard.Insert(ShowCardWaitTime, _spriteRenderer.DOColor(Color.clear, 0.4f)); 
    }
}
