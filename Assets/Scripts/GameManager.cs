﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    [SerializeField] private DeckOrganizer deckOrganizer;
    [SerializeField] private UIManager uIManager;

    [SerializeField] private Card _selectedCard;
    [SerializeField] private float remainingTime;

    [SerializeField]
    private enum GameState
    {
        reveal,
        gaming,
        endGame
    }
    private GameState gameState = GameState.reveal;

    private int _score = 0;
    private int _comboCounter = 1;
    private int _remainingCards = 0;
    private float _levelTotalTime;
    private UnityAction<int, int> _onGameWin;
    private UnityAction _onGameLose;
    
    
    public void InitialGame(Level level, UnityAction<int, int> OnWin, UnityAction OnLose)
    {
        _selectedCard = null;
        _comboCounter = 1;
        _score = 0;
        gameState = GameState.reveal;
        deckOrganizer.OrganizeDeck(level);
        remainingTime = level.LevelTime;
        _levelTotalTime = level.LevelTime;
        _remainingCards = level.cardsCount;
        uIManager.OnSetTime(remainingTime / _levelTotalTime);
        uIManager.OnSetScore(_score);
        _onGameWin = OnWin;
        _onGameLose = OnLose;
        Invoke("RevealCards", 1);
    }

    public void RevealCards()
    {
        deckOrganizer.RevealDeck(() => Invoke("StartGame", 2));
    }

    public void StartGame()
    {
        gameState = GameState.gaming;
        uIManager.ShowGameState("Begin!");
    }

    public void FixedUpdate()
    {
        if (gameState == GameState.gaming)
        {
            remainingTime -= Time.deltaTime;
            if (remainingTime <= 0)
            {
                OnTimesUp();
            }
            uIManager.OnSetTime(remainingTime / _levelTotalTime);
        }
    }
    

    public void OnTimesUp()
    {
        gameState = GameState.endGame;
        uIManager.ShowGameState("Game Over!");
        deckOrganizer.CollectDeckCards(() =>  _onGameLose?.Invoke());
       

    }

    public void OnWin()
    {
        gameState = GameState.endGame;
        uIManager.ShowGameState("Victory :D");
        deckOrganizer.CollectDeckCards(() =>  _onGameWin?.Invoke(_score + (int)remainingTime, _score));
    }
    
    public void CardClick(Card card)
    {
        if (gameState != GameState.gaming)
        {
            return;
        }
        if (_selectedCard == null)
        {
            _selectedCard = card;
            card.FlipFront();
        } else if (card == _selectedCard)
        {
            SameCards(card);
        } else if (_selectedCard.cardType != card.cardType)
        {
            WrongCards(card);
        } else
        {
            MatchCards(card);
        }
    }

    public void SameCards(Card card)
    {
        card.FlipBack();
        remainingTime -= 1;
        uIManager.OnAlertTime();
        _selectedCard = null;
    }

    public void WrongCards(Card card)
    {
        card.RevealCard();
        _selectedCard.FlipBackWithWait();
        _selectedCard = null;
        remainingTime -= 2;
        uIManager.OnAlertTime();
        _comboCounter = 1;
    }

    public void MatchCards(Card card)
    {
        _selectedCard.Matched();
        card.Matched();
        _score += 2 * _comboCounter;
        if (_comboCounter > 1)
        {
            uIManager.ShowGameState("COMBO " + _comboCounter + "X");
        }
        uIManager.OnSetScore(_score);
        _comboCounter += 1;
        _selectedCard = null;
        _remainingCards -= 2;
        if (_remainingCards <= 0)
        {
            OnWin();
        }
    }


}
