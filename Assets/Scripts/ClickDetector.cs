﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickDetector : MonoBehaviour
{

    private GameManager _gameManager;
    private Camera _camera;
    private RaycastHit2D hit;
    private Vector3 _mousePosition;
    private Vector2 _mousePosition2D;
    void Start()
    {
        _camera = Camera.main;
        _gameManager = transform.GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _mousePosition = _camera.ScreenToWorldPoint(Input.mousePosition);
            _mousePosition2D.x = _mousePosition.x;
            _mousePosition2D.y = _mousePosition.y;

            hit = Physics2D.Raycast(_mousePosition2D, Vector2.zero);
            if (hit.collider != null)
            {
                hit.collider.GetComponent<Card>().OnClick();
            }
        }
    }
}
