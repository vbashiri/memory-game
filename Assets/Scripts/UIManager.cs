﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private LevelManager levelManager;
    [SerializeField] private Text coinIndicator;
    [SerializeField] private Text scoreIndicator;
    [SerializeField] private Text gameStateIndicator;
    [SerializeField] private Text coinMainIndicator;
    [SerializeField] private Text highScoreIndicator;
    [SerializeField] private RectTransform gameStateTransform;
    [SerializeField] private Image remainingTime;
    [SerializeField] private Image timeBar;
    
    [SerializeField] private GameObject MainMenuCanvas;
    [SerializeField] private GameObject HudCanvas;
    
    private Sequence _disappearText;
    private Sequence _alertTimeBar;
    
    public void  Start()
    {
        InitialDisappearAnimation();
        InitialAlertAnimation();
    }

    public void OnPlayClick()
    {
        MainMenuCanvas.SetActive(false);
        HudCanvas.SetActive(true);
        levelManager.InitialLevel();
    }

    public void SetHighScore(int score)
    {
        highScoreIndicator.text = score + "";
    }

    public void OnSetCoin(int coin)
    {
        coinMainIndicator.text = coin + "";
        coinIndicator.text = coin + "";
    }

    public void OnExitClick()
    {
        Application.Quit();
    }

    public void OnSetTime(float remaining)
    {
        remainingTime.fillAmount = remaining;
    }

    public void OnSetScore(int score)
    {
        scoreIndicator.text = score + "";
    }

    public void OnAlertTime()
    {
        _alertTimeBar.Restart();
        _alertTimeBar.SetLoops(4, LoopType.Yoyo).PlayForward();
    }

    public void ShowGameState(string message)
    {
        gameStateIndicator.text = message;
        _disappearText.Restart();
        _disappearText.PlayForward();
    }


    private void InitialDisappearAnimation()
    {
        _disappearText = DOTween.Sequence();
        _disappearText.Insert(1f, gameStateIndicator.DOColor(Color.clear, 2f)); 
        _disappearText.Insert(1f, gameStateTransform.DOMove(
            new Vector3(gameStateTransform.position.x, gameStateTransform.position.y + 50f, 0f),
            2f)); 
    }

    private void InitialAlertAnimation()
    {
        _alertTimeBar = DOTween.Sequence();
        _alertTimeBar.Append(timeBar.DOColor(new Color(0.79f, 0.33f, 0.32f, 1f), 0.3f));
        
    }
}
