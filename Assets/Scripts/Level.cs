﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level", menuName = "ScriptableObjects/Level", order = 1)]
public class Level : ScriptableObject
{
    public int cardsCount = 6;
    public float LevelTime = 10f;
    public ColorPalette colorPallete;
}
